﻿namespace HRInterface_Console
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;

    using Dapper;
    using NLog;
    using Oracle.ManagedDataAccess.Client;

    public class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static ICommonRepository commonRepository;

        public static void Main(string[] args)
        {
            StructureMapContainer.Configure();
            commonRepository = StructureMapContainer.Container.GetInstance<ICommonRepository>();

            //logger.Info("Process Entry Point");
            Console.WriteLine("Process Entry Point");
            try
            {
                Console.WriteLine("Getting Process Log Data");
                var data = ProcessTransfer();
                Console.WriteLine("Total Number of Records found " + data.Count());
                Console.WriteLine("Getting Records Completed");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                logger.Error($"Error Message " + ex.Message);
                logger.Error($"Error Stack Trace " + ex.StackTrace.ToString());
                Console.ReadLine();
            }
        }

        public static IEnumerable<ProcessLogModel> ProcessTransfer()
        {           
            using (var uow = new UnitOfWork(commonRepository))
            {
                return uow.CommonRepository.GetProcessLog();
            }
        }
       
    }
}
