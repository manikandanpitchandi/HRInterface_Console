﻿namespace HRInterface_Console
{
    using System;

    public class GetPersonalDataRequest
    {
        public GetPersonalDataRequest()
        {
        }

        public int pdseq { get; set; }

        public string EMPLID { get; set; }

        public DateTime EFFDT { get; set; }

        public DateTime BIRTHDATE { get; set; }

        public string NAME_PREFIX { get; set; }

        public string FIRST_NAME { get; set; }

        public string MIDDLE_NAME { get; set; }

        public string LAST_NAME { get; set; }

        public string NAME_SUFFIX { get; set; }

        public string MAR_STATUS { get; set; }

        public string SEX { get; set; }

        public string NATIONAL_ID { get; set; }

        public string ADDRESS1 { get; set; }

        public string ADDRESS2 { get; set; }

        public string ADDRESS3 { get; set; }

        public string CITY { get; set; }

        public string STATE { get; set; }

        public string POSTAL { get; set; }

        public string COUNTY { get; set; }

        public string EMAIL_ADDR { get; set; }

        public string MAIL_DROP { get; set; }

        public string TRANSFER_SEQ { get; set; }
    }
}