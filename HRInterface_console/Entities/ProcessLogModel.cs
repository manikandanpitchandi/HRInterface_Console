﻿namespace HRInterface_Console
{
    public class ProcessLogModel
    {
        public ProcessLogModel()
        {
        }

        public string TRANSFER_SEQ { get; set; }

        public string OBJECT_CODE { get; set; }

        public string OBJECT_SEQ { get; set; }

        public string EFFDT { get; set; }

        public string RESPONSE_MSG { get; set; }

        public string PS_STATUS { get; set; }

        public string DT_TRANSFER { get; set; }
    }
}