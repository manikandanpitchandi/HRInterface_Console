﻿using System;

namespace HRInterface_console
{

    public class ReturnType
    {
        public string ReturnMessage { get; set; }
        public int? ReturnCode { get; set; }
        public string ReturnOutput { get; set; }

        public ReturnType()
        {
        }

        public ReturnType(IReturnType rt)
        {
            ReturnMessage = rt.ReturnMessage;
            ReturnCode = rt.ReturnCode;
            ReturnOutput = rt.ReturnOutput;
        }

        public ReturnType(int returnCode, string returnMessage, string returnOutput)
        {
            ReturnMessage = returnMessage;
            ReturnCode = returnCode;
            ReturnOutput = returnOutput;
        }


    }

    [Serializable]
    public class ListColumn
    {
        public string value { get; set; }
        public string text { get; set; }
    }
}
