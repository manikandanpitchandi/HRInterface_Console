﻿namespace HRInterface_Console
{
    using System;

    public class GetRetirementData
    {
        public GetRetirementData()
        {
        }

        public string EMPLID { get; set; }

        public DateTime EFFDT { get; set; }

        public DateTime CE_DT { get; set; }

        public string PLNTYPE { get; set; }

        public string BENEFIT_PLAN { get; set; }

        public string CE { get; set; }

        public string TSEQ { get; set; }
    }
}
