﻿namespace HRInterface_Console
{
    public class GetPositionRequest
    {
        public GetPositionRequest()
        {
        }

        ////public string ReportsTo { get; set; }
        ////public string Location { get; set; }
        ////public string CI_Action { get; set; }

        public string POSITION_NBR { get; set; }

        public string EFFDT { get; set; }

        public string EFF_STATUS { get; set; }

        public string DESCR { get; set; }

        public string ACTION_REASON { get; set; }

        public string BUSINESS_UNIT { get; set; }

        public string DEPTID { get; set; }

        public string JOBCODE { get; set; }

        public string STD_HOURS { get; set; }

        public string UNION_CD { get; set; }

        public string REG_TEMP { get; set; }

        public string FULL_PART_TIME { get; set; }

        public string SAL_ADMIN_PLAN { get; set; }

        public string PBC_POS_DATA_WS { get; set; }

        public string POS_SEQ { get; set; }

        public string ps_pos_seq { get; set; }
    }
}