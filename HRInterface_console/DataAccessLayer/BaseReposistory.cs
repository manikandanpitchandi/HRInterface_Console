﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

public class BaseRepository
{
    IDbConnection connection;
    public BaseRepository()
    {
    }
    protected T QueryFirstOrDefault<T>(string sql, object parameters = null)
    {
        using (var connection = CreateConnection())
        {
            this.setConnection();
            return connection.QueryFirstOrDefault<T>(sql, parameters);
        }
    }

    protected List<T> Query<T>(string sql, object parameters = null)
    {
        using (var connection = CreateConnection())
        {
            this.setConnection();
            return connection.Query<T>(sql, parameters).ToList();
        }
    }

    protected int Execute(string sql, object parameters = null)
    {
        using (var connection = CreateConnection())
        {
            this.setConnection();
            return connection.Execute(sql, parameters);
        }
    }

    private IDbConnection CreateConnection()
    {
        var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        if (!string.IsNullOrEmpty(connectionString))
        {
            this.connection = new OracleConnection(connectionString);
        }

        return this.connection;
    }

    private void setConnection()
    {
        if (this.connection == null)
        {
            CreateConnection();
        }

        if (this.connection.State != ConnectionState.Open)
        {
            this.connection.Open();
        }
    }

}