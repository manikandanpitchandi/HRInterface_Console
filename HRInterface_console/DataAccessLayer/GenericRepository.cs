﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace HRInterface_console
{
    public abstract class GenericRepository<T> : IRepository<T> where T : class
    {
        public DAL dal;

        public string _appCode;
        public string _appBase;
        public string _pwd;
        public string _ConStr;
        public List<DBParams> param;
        
        protected GenericRepository()
        {
            //_appCode = Settings.AppCode.ToLower() + "_connect";

            _appCode = "PBCHRMS";
            _appBase = "EWAS";//Settings.AppBase.ToLower();

            //_pwd = Security.Security_Encryption(_appCode, "PBCSECURITY", _appBase);

            _pwd = "pbr3e5as";
            //_pwd = "countyhris";//"hg126nk";

            _ConStr = "Data Source=" + _appBase + ";User Id=" + _appCode + ";Password=" + _pwd + ";";
            dal = new DAL(_ConStr);
            param = new List<DBParams>();
        }

        public virtual IEnumerable<T> GetAll(params object[] list)
        {
            return new List<T>();
        }

        public virtual T GetById(object Id)
        {
            T t = default(T);
            return t;
        }

        public virtual ReturnType Save(T t)
        {
            return new ReturnType();
        }

        public virtual ReturnType Delete(string Id)
        {
            return new ReturnType();
        }

        public virtual IEnumerable<ListColumn> GetListByID(object Id)
        {
            return new List<ListColumn>();
        }

        public virtual IEnumerable<T> GetEntityListByID(object Id)
        {
            return new List<T>();
        }

        public List<ListColumn> GetSelectList(DataView dv, string value1, string text1)
        {
            List<ListColumn> lc = new List<ListColumn>();

            lc = dv.Table.AsEnumerable().Select(row => new ListColumn
            {
                value = Convert.ToString(row[value1]),
                text = Convert.ToString(row[text1])

            }).ToList();

            return lc;
        }

        public List<ListColumn> GetDropDownList(string ProcName, string value, string text)
        {
            return GetSelectList(dal.getDVProc(ProcName), value, text);
        }
    }
}
