﻿namespace HRInterface_Console
{
    using System.Collections.Generic;

    public interface ICommonRepository
    {
        IEnumerable<ProcessLogModel> GetProcessLog();
    }  
}
