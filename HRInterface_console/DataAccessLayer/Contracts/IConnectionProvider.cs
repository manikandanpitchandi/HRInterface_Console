﻿namespace HRInterface_Console
{
    public interface IConnectionStringProvider
    {
        string GetConnectionString();
    }
}
