﻿namespace HRInterface_Console
{
    using System;

    public interface IUnitOfWork : IDisposable
    {
        ICommonRepository CommonRepository { get; }

        void Commit();
    }
}
