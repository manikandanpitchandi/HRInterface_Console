﻿namespace HRInterface_Console
{
    using System;
    using System.Data;

    public class UnitOfWork : IUnitOfWork
    {
        private IDbConnection connection;
        private IDbTransaction transaction;
        private ICommonRepository commonRepository;
        private bool disposed;

        public UnitOfWork(ICommonRepository commonRepository)
        {
            ////this.connectionStringProvider = connectionStringProvider;
            ////connection = new OracleConnection(this.connectionStringProvider.GetConnectionString());
            ////connection.Open();
            ////this.transaction = connection.BeginTransaction();
            
            this.commonRepository = commonRepository;
            this.transaction = ((RepositoryBase)commonRepository).Transaction;
            this.connection = ((RepositoryBase)commonRepository).Connection;
        }

        ~UnitOfWork()
        {
            this.Dispose(false);
        }

        public ICommonRepository CommonRepository
        {
            ////get { return commonRepository ?? (commonRepository = new CommonRepository(this.transaction)); }
            get
            {
                return this.commonRepository;
            }
        }

        public void Commit()
        {
            try
            {
                this.transaction.Commit();
            }
            catch
            {
                this.transaction.Rollback();
                throw;
            }
            finally
            {
                this.transaction.Dispose();
                this.transaction = this.connection.BeginTransaction();
                this.ResetRepositories();
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void ResetRepositories()
        {
            this.commonRepository = null;
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (this.transaction != null)
                    {
                        this.transaction.Dispose();
                        this.transaction = null;
                    }

                    if (this.connection != null)
                    {
                        this.connection.Dispose();
                        this.connection = null;
                    }
                }

                this.disposed = true;
            }
        }      
    }
}
