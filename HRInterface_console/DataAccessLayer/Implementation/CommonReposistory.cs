﻿namespace HRInterface_Console
{
    using System.Collections.Generic;
    using System.Data;
    using Dapper;
    using Oracle.ManagedDataAccess.Client;

    public class CommonRepository : RepositoryBase, ICommonRepository
    {
        public CommonRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public IEnumerable<ProcessLogModel> GetProcessLog()
        {

            var parameter = new OracleDynamicParameters();
            parameter.Add("arg_cursor", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            
            const string StoredProcedure = "PBCHRMS.PKG_WEB_SERVICE.GET_TEST_PROCESS_LOG";
            return Connection.Query<ProcessLogModel>(StoredProcedure, param: parameter, commandType: CommandType.StoredProcedure);
        }
    }
}