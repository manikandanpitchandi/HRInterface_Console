﻿namespace HRInterface_Console
{
    using System.Data;

    public abstract class RepositoryBase
    {
        public RepositoryBase(IDbTransaction transaction)
        {
            this.Transaction = transaction;
        }

        public IDbTransaction Transaction { get; private set; }

        public IDbConnection Connection
        {
            get
            {
                return this.Transaction.Connection;
            }
        }
    }
}
