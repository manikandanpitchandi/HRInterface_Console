﻿namespace HRInterface_Console
{
    using System.Configuration;

    public class ConnectionStringProvider : IConnectionStringProvider
    {
        public string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString();
        }
    }
}
