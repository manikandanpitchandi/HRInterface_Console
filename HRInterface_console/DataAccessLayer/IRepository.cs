﻿using System.Collections.Generic;

namespace HRInterface_console
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll(params object[] list);
        T GetById(object Id);
        ReturnType Save(T Entity);
        ReturnType Delete(string Id);
    }
}
