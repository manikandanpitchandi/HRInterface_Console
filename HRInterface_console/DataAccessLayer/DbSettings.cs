﻿using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace HRInterface_console
{
    public class DbSettings
    {
        public string ConnectionString { get; set; }
        public string CursorName { get; set; }

        public DbSettings()
        {
            CursorName = "arg_cursor";
        }

        public DbSettings(string connectionString, string cursorName)
        {
            ConnectionString = connectionString;
            CursorName = cursorName;
        }
    }



    public class DBParams
    {
        public string ParamName { get; set; }
        public object ParamValue { get; set; }
        public ParameterDirection ParamDirection { get; set; }
        public OracleDbType typeOracle { get; set; }
        public int size { get; set; }

        public DBParams(string ParamName, string type)
        {
            this.ParamName = ParamName;
            this.ParamValue = null;
            this.ParamDirection = ParameterDirection.Output;
            this.typeOracle = getParamType(type);
            if (type == "C")
                this.size = 0;
            else
                this.size = 1000;
        }

        public DBParams(string ParamName, string type, int size)
        {
            this.ParamName = ParamName;
            this.size = size;
            this.ParamDirection = ParameterDirection.Output;
            this.typeOracle = getParamType(type);
        }

        public DBParams(string ParamName, object ParamValue, string type, int size=4000)
        {
            this.ParamName = ParamName;
            this.ParamValue = ParamValue;
            this.ParamDirection = ParameterDirection.Input;
            this.typeOracle = getParamType(type);

            if (type == "D")
                this.size = 0;
            else
                this.size = size;
        }

        public DBParams(string name, string type, int size, ParameterDirection direction, object value)
        {
            this.ParamName = name;
            this.typeOracle = getParamType(type);
            this.ParamValue = value;
            this.size = size;
            this.ParamDirection = direction;
        }

        private OracleDbType getParamType(string strType)
        {
            OracleDbType varType = new OracleDbType();
            switch (strType)
            {
                case "V":
                    varType = OracleDbType.NVarchar2;
                    break;
                case "C":
                    varType = OracleDbType.RefCursor;
                    break;
                case "N":
                    varType = OracleDbType.Decimal;
                    break;
                case "I":
                    varType = OracleDbType.Int32;
                    break;
                case "D":
                    varType = OracleDbType.Date;
                    break;
                case "B":
                    varType = OracleDbType.Blob;
                    break;
                case "SI":
                    varType = OracleDbType.Int16;
                    break;
                case "DC":
                    varType = OracleDbType.Single;
                    break;
                case "DB":
                    varType = OracleDbType.Double;
                    break;
                case "CL":
                    varType = OracleDbType.Clob;

                    break;
            }
            return varType;
        }
    }



    public interface IReturnType
    {

        string ReturnMessage { get; set; }
        int? ReturnCode { get; set; }
        string ReturnOutput { get; set; }
    }


    public class DALReturnType : IReturnType
    {

        public string ReturnMessage { get; set; }
        public int? ReturnCode { get; set; }

        public string ReturnOutput { get; set; }
    }

}
