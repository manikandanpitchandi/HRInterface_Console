﻿namespace HRInterface_Console
{
    using System.Data;
    using Oracle.ManagedDataAccess.Client;
    using StructureMap;
    
    public static class StructureMapContainer
    {
        private static IContainer container = new Container();

        public static IContainer Container
        {
            get
            {
                return container;
            }
        }

        public static void Configure()
        {
            container.Configure(x => x.For<IConnectionStringProvider>().Use<ConnectionStringProvider>().Singleton());
            container.Configure(x => x.For<ICommonRepository>().Use<CommonRepository>().Ctor<IDbTransaction>().Is(GetTransaction()));
        }

        public static IDbTransaction GetTransaction()
        {
            var connectionStringProvider = StructureMapContainer.Container.GetInstance<IConnectionStringProvider>();
            var connection = new OracleConnection(connectionStringProvider.GetConnectionString());
            connection.Open();
            return connection.BeginTransaction();
        }
    }
}
